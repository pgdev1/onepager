$(
    fit_header()
)
$(window).on('resize' , function ()
                        {
                            fit_header();
                        }
)
$('.nav-link').on('click',  function (ele)
                            {
                                set_all_links_inactive();
                                set_link_active(ele);
                            }
)                        
$('#title_link').on('click',  function ()
                            {
                                set_all_links_inactive();
                            }
)                        
$(".hamburger").on('click', function(){
    $(this).toggleClass("active_mob_nav");
})
$('.nav-link,#main_wrapper').on('click', function(){
    $(".hamburger").removeClass("active_mob_nav");
})


function fit_header() {
    const navHeight  = $('.align_nav').outerHeight();

    // Select all h2 elements on the page
    const h2Elements = document.querySelectorAll('h2');

    // Initialize variables to keep track of the maximum height (including margin) and the biggest h2 element
    let maxH2Height = 0;
    let biggestH2 = null;

    // Iterate through each h2 element
    h2Elements.forEach(h2 => {
        // Get the computed style of the current h2 element
        const computedStyle = window.getComputedStyle(h2);

        // Calculate the total height of the current h2 element including margin
        const heightWithMargin = h2.offsetHeight + parseInt(computedStyle.marginTop) + parseInt(computedStyle.marginBottom);

        // Update maxH2Height if the current h2 element's height with margin is greater
        if (heightWithMargin > maxH2Height) {
            maxH2Height = heightWithMargin;
        }
    });

    // trunc result to prevent gap between header and main div
    const scrollOffset = navHeight + maxH2Height - 3 ;
    console.log('scrollOffset', scrollOffset)
    $('#nav_dumy').css({"height": navHeight + "px"});
    $('.scroll_offset').css({"transform": "translateY( -" +  scrollOffset + "px)"});
}

function set_all_links_inactive(){
    $(".nav-link").parent().removeClass("active_link");
}

function set_link_active(ele){
    $(ele.target).parent().addClass("active_link");
}