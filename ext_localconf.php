<?php
    defined('TYPO3') or die();

    use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
    use TYPO3\CMS\Core\Core\Environment;

    call_user_func( 
        function(){
            ExtensionManagementUtility::addUserTSConfig(
                'options.pageTree.showPageIdWithTitle = 1',
            );

            // disable cache in development context
            if ( Environment::getContext() == "Development"){
                ExtensionManagementUtility::addUserTSConfig(
                    'admPanel.override.tsdebug.forceTemplateParsing = 1'
                );
            }
        }
    );

    