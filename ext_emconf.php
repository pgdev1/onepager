<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'onepager',
    'description' => 'Sitepackage for TYPO3 12',
    'category' => 'templates',
    'author' => 'Peter Grueger',
    'author_email' => 'mail@pgwebdev.de',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '12.4.0-12.4.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
