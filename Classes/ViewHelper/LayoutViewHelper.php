<?php
namespace PgDev\onepager\ViewHelper;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithContentArgumentAndRenderStatic;

/**
 * Orientation assigning ViewHelper is copied from f:variable viewhelper
 * 
 * 
 * Renders a appropriate css-class from the given content-item.data.imageorient
 *
 * Usages:
 *
 *     {pgd:layout(name: 'myvariable', value: '{content-item.data.imageorient}')}
 *     <pgd:layout name="myvariable">{content-item.data.imageorient}</pgd:layout>
 */
class LayoutViewHelper extends AbstractViewHelper 
{

    use CompileWithContentArgumentAndRenderStatic;

    public function initializeArguments()
    {
        $this->registerArgument('orientation', 'mixed', 'Value from ContentElement.data.imageorient');
        $this->registerArgument('name', 'string', 'Name of variable to create', true);
    }

    public static function renderStatic(
        array $arguments, 
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ){
        $orientation = $renderChildrenClosure();
        $class = "";
        switch($orientation) {
            case 0:
                $class = "above center";
                break;
            case 1:
                $class = "above right";
                break;
            case 2:
                $class = "above left";
                break;
            case 8:
                $class = "below center";
                break;
            case 9:
                $class = "below right";
                break;
            case 10:
                $class = "below left";
                break;
            case 17:
                $class = "in_text float_right";
                break;
            case 18:
                $class = "in_text float_left";
                break;
            case 25:
                $class = "beside_text_right";
                break;
            case 26:
                $class = "beside_text_left";
                break;
            default:
                $class = "beside_text_left";

        }
        $renderingContext->getVariableProvider()->add($arguments['name'], $class);
    }
}