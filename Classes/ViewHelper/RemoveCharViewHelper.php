<?php
namespace PgDev\onepager\ViewHelper;

use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;


class RemoveCharViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        $this->registerArgument('string2clean', 'string', 'The input string', true);
        $this->registerArgument('char2remove', 'string', 'The character to remove', true);
    }

    public function render()
    {
        $string2clean = $this->arguments['string2clean'];
        $char2remove = $this->arguments['char2remove'];

        return str_replace($char2remove, '', $string2clean);
    }
}
