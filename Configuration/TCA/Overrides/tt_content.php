<?php
// https://docs.typo3.org/m/typo3/reference-coreapi/12.4/en-us/ApiOverview/ContentElements/AddingYourOwnContentElements.html
// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
   'tt_content',
   'CType',
    [
        // title
        // 'LLL:EXT:examples/Resources/Private/Language/locallang.xlf:examples_newcontentelement_title',
        'BG-Image',
        // plugin signature: extkey_identifier
        'pg_background',
        // icon identifier
        'content-image',
    ],
    'textmedia',
    'after'
);

// Configure the default backend fields for the content element
// copied from vendor/typo3/cms-frontend/Configuration/TCA/tt_content.php line 1424
$GLOBALS['TCA']['tt_content']['types']['pg_background'] = [
    'previewRenderer' => \TYPO3\CMS\Frontend\Preview\ImagePreviewRenderer::class,
    'showitem' => '
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:general,
            --palette--;;general,
            --palette--;;headers,
            bodytext;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:bodytext_formlabel,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.images,
            image,
            --palette--;;mediaAdjustments,
            --palette--;;gallerySettings,
            --palette--;;imagelinks,
        --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
            --palette--;;frames,
            --palette--;;appearanceLinks,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
            --palette--;;language,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access,
            --palette--;;hidden,
            --palette--;;access,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:categories,
            categories,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:notes,
            rowDescription,
        --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:extended,
    ',
    'columnsOverrides' => [
        'bodytext' => [
            'config' => [
                'enableRichtext' => true,
            ],
        ],
    ],
 ];